// © 2016 George King. Permission to use this file is granted in license-quilt.txt.

@testable import Quilt
import XCTest

class Tests: XCTestCase {

  // functions beginning with 'test' are automatically run by `swift test`.
  func testA() {}
  func testB() {}
}
