// © 2014 George King. Permission to use this file is granted in license-quilt.txt.

import Foundation


extension NotificationCenter {}

public let noteCenter: NotificationCenter = NotificationCenter.default
